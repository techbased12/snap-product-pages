<?php
/*
Plugin Name: Snap Product Pages
Plugin URI: http://PluginMill.com
Description: Creates product pages
Version: 1.0.4
Author: Lynette Chandler
Author URI: http://techbasedmarketing.com
Stable tag: 1.0.4
*/
// Define constants
define( 'PM_SNAPPRO_DIR', WP_PLUGIN_URL.'/pm-product-pages/' );
define( 'PM_SNAPPRO_PATH', WP_PLUGIN_DIR.'/pm-product-pages' );
define( 'PM_SNAPPRO_WP_DIR', get_bloginfo('wpurl') );
define( 'PM_SNAPPRO_PLUGIN_NAME', 'Snap Product Pages' );
define( 'PM_SNAPPRO_VERSION', '1.0.4' );

$pm_snappro_opts = get_option( 'pm_snappro_options' );

include_once('modules/settings-api-tabbed.php');
include_once('modules/cpt.php');
include_once('modules/hooks.php');
include_once('modules/scripts.php');
include_once('modules/widgets.php');

register_activation_hook( __FILE__, 'pm_snappro_activate' );
function pm_snappro_activate() {

 $pm_snappro_options = array(  
  'productslug' =>  'pm_products',  
  );
  if( !get_option('pm_snappro_options') ){
	update_option('pm_snappro_options', $pm_snappro_options );
  }

  flush_rewrite_rules();

}
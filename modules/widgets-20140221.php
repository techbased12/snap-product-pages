<?php 
/*--------------------------------------------
* Widget for Custom recent posts
---------------------------------------------*/
/**
 * Adds PM_Advrp_Widget widget.
 */
class PM_Advrp_Widget extends WP_Widget {

	/**
	 * Register widget with WordPress.
	 */
	function __construct() {
		parent::__construct(
			'pm_advrp_widget', // Base ID
			__('Adv Recent Posts', 'pm_advrp'), // Name
			array( 'description' => __( 'Advanced recent posts for Snappro', 'pm_advrp' ), ) // Args
		);
	}

	/**
	 * Front-end display of widget.
	 *
	 * @see WP_Widget::widget()
	 *
	 * @param array $args     Widget arguments.
	 * @param array $instance Saved values from database.
	 */
	public function widget( $args, $instance ) {
		$title = apply_filters( 'widget_title', $instance['title'] );
		$ptype = strip_tags( $instance['ptype'] );
		$post_per_page = strip_tags( $instance['post_per_page'] );
		$tax_term = strip_tags( $instance['tax_term'] );
		//Protect against arbitrary paged values
		$paged = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;

		// get_posts arguments
		$gpargs = array(
			'posts_per_page' => $post_per_page,
			'category' => '',
			'post_type' => $ptype,
			'paged' => $paged,
			'tax_query' => array(array(
				'taxonomy' => 'type',
				'field' => 'slug',
				'terms' => array( $tax_term )
				))
			);
		$posts_array = new WP_Query( $gpargs );

		echo $args['before_widget'];
		if ( ! empty( $title ) )
			echo $args['before_title'] . $title . $args['after_title'];
		$thumbargs = array(
			'class' => 'attachment-thumbnail aligncenter'
			);
		// Show posts
		if( $posts_array->have_posts() ) {
			$thumbargs = array(
			'class' => 'attachment-thumbnail aligncenter'
			);
			echo "<ul>";
			while( $posts_array->have_posts() ) {
				$posts_array->the_post();
				$excerpt = substr(get_the_content(), 0, 200).'...';
				$post_id = get_the_ID();
				$post_type = get_post_type();
				$post_status = get_post_status();
				?>
				<div class="post-<?php echo $post_id; ?> <?php echo $post_type; ?> type-<?php echo $post_type; ?> status-<?php echo $post_status; ?> entry pm_advrp">
					<div class="product-container">
						<a href="<?php echo get_permalink(); ?>" class="product-cover">
							<?php echo get_the_post_thumbnail( $post_id, 'thumbnail', $thumbargs ); ?>
							<h3><?php echo get_the_title(); ?></h3>
						</a>
						<a href="<?php echo get_permalink(); ?>" class="product-info">
							<?php echo strip_tags($excerpt); ?>
							<div class="product-item-button">See Details</div>
						</a>
					</div>
				</div>
				<?php
			}
			echo "</ul>";
		} else {
			echo "No posts found";
		}

		$big = 999999999; // need an unlikely integer

		echo paginate_links( array(
			'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
			'format' => '?paged=%#%',
			'current' => max( 1, get_query_var('paged') ),
			'total' => $posts_array->max_num_pages
		) );
		wp_reset_postdata();
		echo $args['after_widget'];
	}

	/**
	 * Back-end widget form.
	 *
	 * @see WP_Widget::form()
	 *
	 * @param array $instance Previously saved values from database.
	 */
	public function form( $instance ) {
		$pm_advrp_all_ptypes = get_post_types( array( 'public'=> true ) );
		$tax_terms = get_terms( 'type' );
		if ( isset( $instance[ 'title' ] ) ) {
			$title = $instance[ 'title' ];
		}
		else {
			$title = __( 'New title', 'pm_advrp' );
		}
		$post_per_page = ( !empty( $instance['post_per_page'] ) ) ? $instance['post_per_page'] : '' ;
		?>
		<p>
		<label for="<?php echo $this->get_field_id( 'title' ); ?>"><?php _e( 'Title:' ); ?></label> 
		<input class="widefat" id="<?php echo $this->get_field_id( 'title' ); ?>" name="<?php echo $this->get_field_name( 'title' ); ?>" type="text" value="<?php echo esc_attr( $title ); ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'ptype' ); ?>"><?php _e( 'Post type:' ); ?></label>
			<select name="<?php echo $this->get_field_name( 'ptype' ); ?>" id="<?php echo $this->get_field_id( 'ptype' ); ?>">
				<?php 
				foreach( $pm_advrp_all_ptypes as $post_type ){
					echo '<option value="'.$post_type.'" '.selected( $instance['ptype'], $post_type, true ).'>'.$post_type.'</option>';
				}
				?>
			</select>
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'post_per_page' ); ?>"><?php _e( 'Posts per page:' ); ?></label>
			<input class="widefat" id="<?php echo $this->get_field_id( 'post_per_page' ); ?>" name="<?php echo $this->get_field_name( 'post_per_page' ); ?>" type="text" value="<?php echo esc_attr($post_per_page ); ?>">
		</p>
		<p>
			<label for="<?php echo $this->get_field_id( 'tax_term' ); ?>"><?php _e( 'Post type:' ); ?></label>
			<select name="<?php echo $this->get_field_name( 'tax_term' ); ?>" id="<?php echo $this->get_field_id( 'tax_term' ); ?>">
				<?php 
				foreach( $tax_terms as $tax_term ){
					echo '<option value="'.$tax_term->slug.'" '.selected( $instance['tax_term'], $tax_term->slug, true ).'>'.$tax_term->name.'</option>';
				}
				?>
			</select>
		</p>
		<?php 
	}

	/**
	 * Sanitize widget form values as they are saved.
	 *
	 * @see WP_Widget::update()
	 *
	 * @param array $new_instance Values just sent to be saved.
	 * @param array $old_instance Previously saved values from database.
	 *
	 * @return array Updated safe values to be saved.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance = array();
		$instance['title'] = ( ! empty( $new_instance['title'] ) ) ? strip_tags( $new_instance['title'] ) : '';
		$instance['ptype'] = ( ! empty( $new_instance['ptype'] ) ) ? strip_tags( $new_instance['ptype'] ) : 'error';
		$instance['post_per_page'] = ( !empty( $new_instance['post_per_page'] ) && is_numeric( $new_instance['post_per_page'] ) ) ? strip_tags( $new_instance['post_per_page'] ) : '' ;
		$instance['tax_term'] = ( !empty( $new_instance['tax_term'] ) ) ? strip_tags( $new_instance['tax_term'] ) : '' ;

		return $instance;
	}

} // class PM_Advrp_Widget

// register PM_Advrp_Widget widget
function register_pm_advrp_widget() {
    register_widget( 'PM_Advrp_Widget' );
}
add_action( 'widgets_init', 'register_pm_advrp_widget' );
?>
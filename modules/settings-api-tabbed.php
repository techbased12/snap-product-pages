<?php
/* References
 * https://github.com/chipbennett/oenology/blob/master/functions/options.php#L84
 * http://www.chipbennett.net/2011/02/17/incorporating-the-settings-api-in-wordpress-themes/7/
 */
/* Add admin menu */
add_action( 'admin_menu', 'pm_snappro_admin_menu' );
function pm_snappro_admin_menu() {
	global $pm_snappro_opts;
	// Create options page
	$settings = add_submenu_page('edit.php?post_type=pm_products', PM_SNAPPRO_PLUGIN_NAME.' Settings', 'Settings', 'manage_options', 'pm_snappro_settings', 'pm_snappro_options_page');
  	
  	// Load admin scripts when above page is called
  	add_action( 'load-'.$settings, 'pm_snappro_adminscripts' );
}

// Enqueue admin scripts
function pm_snappro_adminscripts() {
	//wp_enqueue_script( 'pm-snappro-admin', PM_SNAPPRO_DIR.'modules/js/build/admin.joined.min.js', array('jquery'), PM_SNAPPRO_VERSION, false );
	//wp_enqueue_style( 'pm-snappro-admin', PM_SNAPPRO_DIR.'modules/css/admin.css', '', PM_SNAPPRO_wpVERSION, 'all' );
}

// Output the settings page
function pm_snappro_options_page() {
	$tab = ( isset( $_GET['tab'] )  ? $_GET['tab'] : 'general' );
	$settings_section = 'pm_snappro_settings_'.$tab.'_tab';
	?>
	<div class="wrap">
		<h2><?php echo PM_SNAPPRO_PLUGIN_NAME; ?> Settings</h2>
		<?php settings_errors(  ); ?>
		<?php pm_snappro_settings_page_tabs(); ?>

		<form action='options.php' method='post'>
			<?php 
			// Call functions to build form
			settings_fields( 'pm_snappro_options' );
			//do_settings_sections( 'pm_snappro_settings' );
			do_settings_sections( $settings_section );

			// Update settings based on tab			
			?>
			<p class="submit">
			<?php
			submit_button( 'Save Settings', 'primary', 'pm_snappro_options[submit-'.$tab.']', false, array( 'id' => 'submit-'.$tab ) );
			submit_button( 'Reset', 'secondary', 'pm_snappro_options[reset-'.$tab.']', false, array( 'id' => 'reset-'.$tab ) );
			?></p>
		</form>
	</div>
	<?php
}

// Create tabs
function pm_snappro_get_settings_page_tabs() {
	$tabs = array(
		'general' => 'General',
		'amember' => 'Amember'
		);
	return $tabs;
}

// HTML Markup for tabs
function pm_snappro_settings_page_tabs( $current = 'general' ) {
	// Determind which tab to display
	if( isset( $_GET['tab'] ) ) :
		$current = $_GET['tab'];
	else:
		$current = 'general';
	endif;

	// Get list of tabs
	$tabs = pm_snappro_get_settings_page_tabs();
	$links = array();

	// Create links for each tab
	foreach ($tabs as $tab => $name) :
		if( $tab == $current ) :
			$links[] = '<a class="nav-tab nav-tab-active" href="?post_type=pm_products&page=pm_snappro_settings&tab='.$tab.'">'.$name.'</a>';
		else :
			$links[] = '<a class="nav-tab" href="?post_type=pm_products&page=pm_snappro_settings&tab='.$tab.'">'.$name.'</a>';
		endif;
	endforeach;

	echo '<h2 class="nav-tab-wrapper">';
	foreach ( $links as $link ) {
		echo $link;
	}
	echo '</h2>';
}

// Register settings
add_action( 'admin_init', 'pm_snappro_initialize_settings' );
function pm_snappro_initialize_settings() {
	// Register fields
	register_setting( 
		'pm_snappro_options',
		'pm_snappro_options', 
		'pm_snappro_validate_options' );

	// Add settings sections
	pm_snappro_add_settings_sections();
	//add_settings_section( 'permanent', 'One Section', 'pm_snappro_one_section_callback', 'pm_snappro_settings' );

	// Add settings fields
	pm_snappro_add_settings_fields();
	//add_settings_field( 'textfield', 'Text Field', 'pm_snappro_textfield_field_callback', 'pm_snappro_settings', 'permanent' );
}

/*---------------
 Sections
 ---------------*/
// Register multiple settings (called in admin_init)
function pm_snappro_add_settings_sections() {
	// Array with settings identifiers
	$sects = array(
		'requirements' => array(
			'id' => 'requirements',
			'title' => 'Default Requirements',
			'page' => 'pm_snappro_settings_general_tab'
			),
		'permanent' => array(
			'id' => 'permanent',
			'title' => 'Permanent Settings',
			'page' => 'pm_snappro_settings_general_tab'
			),
		'upsell' => array(
			'id' => 'upsell',
			'title' => 'Upsell Settings',
			'page' => 'pm_snappro_settings_general_tab'
			),
		'ambrsect' => array(
			'id' => 'ambrsect',
			'title' => 'aMember Settings',
			'page' => 'pm_snappro_settings_amember_tab'
			)
		);

	// Loop through each
	foreach ($sects as $s) {
		add_settings_section( 
			$s['id'].'_section',
			$s['title'].' Options',
			'pm_snappro_'.$s['id'].'_section_callback',
			$s['page']
			);
	}
}

/*----------------
Sections callback
-----------------*/
// Section callback
function pm_snappro_requirements_section_callback() {
	echo "<p>These are default product requirements if not set on per product basis.</p>";
}
function pm_snappro_permanent_section_callback() {
	echo '';
}
function pm_snappro_upsell_section_callback() {}
function pm_snappro_ambrsect_section_callback() {
	echo '<p>These only apply if you are using aMember for your shopping cart.</p>';
}

/*------------------
Fields
------------------*/
// Register multiple fields (called in admin_init)
function pm_snappro_add_settings_fields () {
	// Array with field info
	$fields = array(
		'phpversion' => array(
			'id' => 'phpversion',
			'title' => 'Required PHP Version',
			'page' => 'pm_snappro_settings_general_tab',
			'section' => 'requirements_section'
			),
		'wpversion' => array(
			'id' => 'wpversion',
			'title' => 'Required WordPress Version',
			'page' => 'pm_snappro_settings_general_tab',
			'section' => 'requirements_section'
			),
		'productslug' => array(
			'id' => 'productslug',
			'title' => 'Page Slug',
			'page' => 'pm_snappro_settings_general_tab',
			'section' => 'permanent_section'
			),
		'personalupsell' => array(
			'id' => 'personalupsell',
			'title' => 'Personal Use Upsell Page',
			'page' => 'pm_snappro_settings_general_tab',
			'section' => 'upsell_section'
			),
		'personaltaxid' => array(
			'id' => 'personaltaxid',
			'title' => 'Personal Use Type ID',
			'page' => 'pm_snappro_settings_general_tab',
			'section' => 'upsell_section'
			),
		'plrupsell' => array(
			'id' => 'plrupsell',
			'title' => 'PLR Upsell Page',
			'page' => 'pm_snappro_settings_general_tab',
			'section' => 'upsell_section'
			),
		'plrtaxid' => array(
			'id' => 'plrtaxid',
			'title' => 'PLR Type ID',
			'page' => 'pm_snappro_settings_general_tab',
			'section' => 'upsell_section'
			),
		'ambrdomain' => array(
			'id' => 'ambrdomain',
			'title' => 'aMember Domain',
			'page' => 'pm_snappro_settings_amember_tab',
			'section' => 'ambrsect_section'
			),
		'ambrfolder' => array(
			'id' => 'ambrfolder',
			'title' => 'aMember Folder',
			'page' => 'pm_snappro_settings_amember_tab',
			'section' => 'ambrsect_section'
			),
		'ambrpath' => array(
			'id' => 'ambrpath',
			'title' => 'aMember Path',
			'page' => 'pm_snappro_settings_amember_tab',
			'section' => 'ambrsect_section'
			)
		);

	// Loop through each in array
	foreach ($fields as $f) {
		add_settings_field( 
			$f['id'],
			$f['title'],
			'pm_snappro_'.$f['id'].'_field_callback',
			$f['page'],
			$f['section']
			);
	}
}

/*--------------------
Fields callback
--------------------*/
function pm_snappro_phpversion_field_callback() {
	global $pm_snappro_opts;
	$phpversion = ( empty( $pm_snappro_opts['phpversion'] ) ) ? '' : esc_attr( $pm_snappro_opts['phpversion'] ); ?>
	<input type="text" name="pm_snappro_options[phpversion]" value="<?php echo $phpversion; ?>" id="phpversion" class="regular-text">
	<?php
}
function pm_snappro_wpversion_field_callback() {
	global $pm_snappro_opts;
	$wpversion = ( empty( $pm_snappro_opts['wpversion'] ) ) ? '' : esc_attr( $pm_snappro_opts['wpversion'] ); ?>
	<input type="text" name="pm_snappro_options[wpversion]" value="<?php echo $wpversion; ?>" id="wpversion" class="regular-text">
	<?php
}
function pm_snappro_productslug_field_callback() {
	// Be sure the field names are formatted for array!
	// Also be sure name does not have ''
	global $pm_snappro_opts;
	$productslug = ( empty( $pm_snappro_opts['productslug'] ) ) ? '' : $pm_snappro_opts['productslug'];
	?>
	<input type="text" name="pm_snappro_options[productslug]" value="<?php echo $productslug; ?>" id="productslug" class="regular-text"><br>
	<small>Choose carefully. This should not be changed later or it will break all your product pages and lose them in the admin.</small>
	<?php
}
function pm_snappro_personalupsell_field_callback(){
	global $pm_snappro_opts;
	$personalupsell = ( empty( $pm_snappro_opts['personalupsell'] ) ) ? '' : stripslashes( $pm_snappro_opts['personalupsell'] ) ; ?>
	<input class="large-text" type="text" name="pm_snappro_options[personalupsell]" value="<?php echo $personalupsell; ?>" data-field="personalupsell">
	<?php
}
function pm_snappro_personaltaxid_field_callback(){
	global $pm_snappro_opts;
	$personaltaxid = ( empty( $pm_snappro_opts['personaltaxid'] ) ) ? '' : stripslashes( $pm_snappro_opts['personaltaxid'] ) ; ?>
	<input class="small-text" type="text" name="pm_snappro_options[personaltaxid]" value="<?php echo $personaltaxid; ?>">
	<?php
}
function pm_snappro_plrupsell_field_callback(){
	global $pm_snappro_opts;
	$plrupsell = ( empty( $pm_snappro_opts['plrupsell'] ) ) ? '' : stripslashes( $pm_snappro_opts['plrupsell'] ) ; ?>
	<input class="large-text" type="text" name="pm_snappro_options[plrupsell]" value="<?php echo $plrupsell; ?>">
	<?php
}
function pm_snappro_plrtaxid_field_callback(){
	global $pm_snappro_opts;
	$plrtaxid = ( empty( $pm_snappro_opts['plrtaxid'] ) ) ? '' : stripslashes( $pm_snappro_opts['plrtaxid'] ) ; ?>
	<input class="small-text" type="text" name="pm_snappro_options[plrtaxid]" value="<?php echo $plrtaxid; ?>">
	<?php
}
function pm_snappro_ambrdomain_field_callback() {
	global $pm_snappro_opts;
	$ambrdomain = ( empty( $pm_snappro_opts['ambrdomain'] ) ) ? '' : $pm_snappro_opts['ambrdomain'];
	?>
	<input type="text" name="pm_snappro_options[ambrdomain]" value="<?php echo $ambrdomain; ?>" id="ambrdomain" class="regular-text">
	<?php
}
function pm_snappro_ambrfolder_field_callback() {
	global $pm_snappro_opts;
	$ambrfolder = ( empty( $pm_snappro_opts['ambrfolder'] ) ) ? '' : $pm_snappro_opts['ambrfolder'];
	?>
	<input type="text" name="pm_snappro_options[ambrfolder]" value="<?php echo $ambrfolder; ?>" id="ambrfolder" class="regular-text">
	<?php
}
function pm_snappro_ambrpath_field_callback() {
	global $pm_snappro_opts;
	$ambrpath = ( empty( $pm_snappro_opts['ambrpath'] ) ) ? '' : esc_attr( $pm_snappro_opts['ambrpath'] );
	?>
	<input type="text" name="pm_snappro_options[ambrpath]" value="<?php echo $ambrpath; ?>" id="ambrpath" class="large-text">
	<?php
}

// Validate options
function pm_snappro_validate_options( $input ) {
	global $pm_snappro_opts;
	$valid_input = $pm_snappro_opts;

	// Determine which tab is submitted
	$general = ( !empty( $input['submit-general'] ) ? true : false );
	$amember = ( !empty( $input['submit-amember'] ) ? true : false );

	if( $general ) {
		// validate only those settings
		$valid_input['wpversion'] = ( isset( $input['wpversion'] ) ) ? sanitize_text_field( $input['wpversion'] ) : '' ;
		$valid_input['phpversion'] = ( isset( $input['phpversion'] ) ) ? sanitize_text_field( $input['phpversion'] ) : '' ;
		$valid_input['productslug'] = ( isset( $input['productslug'] ) ) ? sanitize_text_field( $input['productslug'] ) : '' ;
		$valid_input['personalupsell'] = ( isset( $input['personalupsell'] ) ) ? sanitize_url( $input['personalupsell'] ) : '' ;
		$valid_input['personaltaxid'] = ( isset( $input['personaltaxid'] ) ) ? sanitize_text_field( $input['personaltaxid'] ) : '' ;
		$valid_input['plrupsell'] = ( isset( $input['plrupsell'] ) ) ? sanitize_url( $input['plrupsell'] ) : '' ;
		$valid_input['plrtaxid'] = ( isset( $input['plrtaxid'] ) ) ? sanitize_text_field( $input['plrtaxid'] ) : '' ;

		// reuse ambr settings
		$valid_input['ambrdomain'] = $pm_snappro_opts['ambrdomain'];
		$valid_input['ambrfolder'] = $pm_snappro_opts['ambrfolder'];
		$valid_input['ambrpath'] = $pm_snappro_opts['ambrpath'];

		
	} elseif ( $amember ) {
		// reuse ambr settings
		$valid_input['wpversion'] = $pm_snappro_opts['wpversion'];
		$valid_input['phpversion'] = $pm_snappro_opts['phpversion'];
		$valid_input['productslug'] = $pm_snappro_opts['productslug'];

		// validate only those settings
		$valid_input['ambrdomain'] = ( isset( $input['ambrdomain'] ) ) ? sanitize_text_field( $input['ambrdomain'] ) : '' ;
		$valid_input['ambrfolder'] = ( isset( $input['ambrfolder'] ) ) ? sanitize_text_field( $input['ambrfolder'] ) : '' ;
		$valid_input['ambrpath'] = ( isset( $input['ambrpath'] ) ) ? sanitize_text_field( $input['ambrpath'] ) : '' ;
	}
	return $valid_input;
	/*
	update_option( 'pm_snappro_options', $valid_input );
	print_r($valid_input);
	exit();*/
}
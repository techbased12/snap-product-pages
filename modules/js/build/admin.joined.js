jQuery(document).ready(function($){

	// Datepicker
	$("#updated").datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat: 'd M, yy'
	});

	// Select related product pages
	$("#related").change(function() {
		var str = "";
		$("#related option:selected").each(function() {
		if ( $(this).val() !== '' ) {;
			//alert($(this).val());
				str += "<span class='related_item'>" + $(this).filter(':selected').text() + " (" + $(this).val() + ")";
				str += "<input type='hidden' name='related[]' value='" + $(this).val() + "' class='select_related'>";
				str += "<span id='product-" + $(this).val() + "' class='deleteItem'>X</span></span>";
			$("#related_items").append( str );
		}
		})
	});

	// Deleted products and images
	$('.deleteItem').live('click', function() {
		//$(this).parent().remove();
		$(this).closest('li').remove();
		return false;
	});
	$('.deleteImg').live('click', function() {
		$(this).closest('li').remove();
		return false;
	});

	// Show Image remove button
	$('img.shot').live('click', function() {
		$('.deleteImg').show();
	});

	// Triggers thickbox upload	//http://stackoverflow.com/questions/13847714/wordpress-3-5-custom-media-upload-for-your-theme-options
	$('#pm_snappro_productimg_upload').click(function(e) {
	    e.preventDefault();

	    var product_img_uploader = wp.media({
	        title: 'Upload Product Images',
	        button: {
	            text: 'Insert Images'
	        },
	        multiple: true  // Set this to true to allow multiple files to be selected
	    })
	    .on('select', function() {
	        var selection = product_img_uploader.state().get('selection');
	        selection.map( function( attachment ) {
			    attachment = attachment.toJSON();
			    $('#pm_snappro_product_images').append('<li class="product_shot"><div class="pimage"><img class="shot" src='+attachment.url+'> <span class="deleteImg">x</span> <input type="hidden" name="product_shots['+attachment.id+'][url]" value="'+attachment.url+'"> <input type="hidden" name="product_shots['+attachment.id+'][id]" value="'+attachment.id+'"></div></div></li>');
			});
	    })
	    .open();
	});

}); // main jquery container
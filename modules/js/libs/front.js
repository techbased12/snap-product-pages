jQuery(document).ready( function($){
	$('.screenshots').magnificPopup({
		delegate: 'a',
		type: 'image',
		gallery: {
			enabled: true
		}
	});
	$(".screenshot-nav").jPages({
      containerID: "screenshots",
      previous : "← Prev",
      next : "Next →",
      perPage:8,
      midRange: 3,
      direction: "random",
      animation: "flipInY"
    });
    $('.orderplugin').on('click', function(e) {
    	e.preventDefault();
    	var ambr_id = snappro_ambr_data.ambr_id;

    	if( snappro_ambr_data.upsell === 'on' ) {
    		window.location = snappro_ambr_data.personalupsell+'?pid='+snappro_ambr_data.ambr_id+'&pname='+snappro_ambr_data.pname;
    	} else {
	    	console.log('No upsell '+ambr_id);
	    	cart.addBasketExternal(ambr_id);
			return false;
	    }
    });
}); // global end
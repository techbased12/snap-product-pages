jQuery(document).ready(function($){

	// Datepicker
	$("#updated").datepicker({
		changeMonth: true,
		changeYear: true,
		dateFormat: 'd M, yy'
	});

	// Select related product pages
	$("#related").change(function() {
		var str = "";
		$("#related option:selected").each(function() {
		if ( $(this).val() !== '' ) {
			//alert($(this).val());
				str += "<span class='related_item'>" + $(this).filter(':selected').text() + " (" + $(this).val() + ")";
				str += "<input type='hidden' name='related[]' value='" + $(this).val() + "' class='select_related'>";
				str += "<span id='product-" + $(this).val() + "' class='deleteItem'>X</span></span>";
			$("#related_items").append( str );
		}
		});
	});

	// Deleted products and images
	$('.deleteItem').live('click', function() {
		//$(this).parent().remove();
		$(this).closest('li').remove();
		return false;
	});
	$('.deleteImg').live('click', function() {
		$(this).closest('li').remove();
		return false;
	});

	// Show Image remove button
	$('img.shot').live('click', function() {
		$('.deleteImg').show();
	});

	// Triggers thickbox upload	//http://stackoverflow.com/questions/13847714/wordpress-3-5-custom-media-upload-for-your-theme-options
	$('#pm_snappro_productimg_upload').click(function(e) {
	    e.preventDefault();

	    var product_img_uploader = wp.media({
	        title: 'Upload Product Images',
	        button: {
	            text: 'Insert Images'
	        },
	        multiple: true  // Set this to true to allow multiple files to be selected
	    })
	    .on('select', function() {
	        var selection = product_img_uploader.state().get('selection');
	        selection.map( function( attachment ) {
			    attachment = attachment.toJSON();
			    $('#pm_snappro_product_images').append('<li class="product_shot"><div class="pimage"><img class="shot" src='+attachment.url+'> <span class="deleteImg">x</span> <input type="hidden" name="product_shots['+attachment.id+'][url]" value="'+attachment.url+'"> <input type="hidden" name="product_shots['+attachment.id+'][id]" value="'+attachment.id+'"></div></div></li>');
			});
	    })
	    .open();
	});

	// Quick Edit
	$('a.editinline').on('click', function() {
		// Create copy of WP inline edit post function
		var $wp_inline_edit = inlineEditPost.edit;
		// then we overwrite the function with our own code
		inlineEditPost.edit = function( id ) {
			// call the original WP edit function
			// we don't want to leave WP hanging
			$wp_inline_edit.apply( this, arguments );

			//take care of business, get the post ID
			var $post_id = 0;
			if( typeof( id ) == 'object' )
				$post_id = parseInt( this.getId( id ) );

			if( $post_id > 0 ) {
				// define the edit row
				var $edit_row = $( '#edit-' + $post_id );

				// get the current price
				var $current_price = $( '#current_price-' + $post_id ).text();
				var $current_price = $current_price.replace('$','').trim();

				// get the current wpversion
				var $wpversion = $( '#wpversion-' + $post_id ).text();
				var $wpversion = $wpversion.trim();

				// populate the fields
				$edit_row.find( 'input[name="reg_price"]' ).val( $current_price );
				$edit_row.find( 'input[name="requirements"]' ).val( $wpversion );
			}
		};
	});

	// Bulk Edit
	$('#bulk_edit').on('click', function() {
		// Define bulk edit row
		var $bulk_row = $('#bulk-edit');

		// Get ids of posts being edited
		var $post_ids = new Array();
		$bulk_row.find( '#bulk-titles' ).children().each( function() {
			$post_ids.push( $(this).attr('id').replace(/^(ttle)/i, '') );
		});

		// Get the data
		var $reg_price = $bulk_row.find('input[name="reg_price"]').val();
		var $wpversion = $bulk_row.find('input[name="requirements"]').val();

		// Save the data
		$.ajax({
			url: ajaxurl,
			type: 'POST',
			data: {
				action: 'pm_snappro_save_bulk_edit',
				post_ids: $post_ids,
				reg_price: $reg_price,
				wpversion: $wpversion
			}
		});
	});
});
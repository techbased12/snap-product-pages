<?php
/*--------------------------------------------
* Adds Font Icon to CPT Menu
---------------------------------------------*/
function pm_snappro_cptmenuicon() {
	?>
	<style type="text/css">
		#adminmenu #menu-posts-pm_products div.wp-menu-image:before {
			font-family: pm-product-pages !important;
			content: '\e800';
		}
	</style>
	<?php
}
add_action( 'admin_head', 'pm_snappro_cptmenuicon' );

function pm_snappro_admin_font_init() {
	wp_enqueue_style( 'pm-snappro-icon', PM_SNAPPRO_DIR.'modules/css/libs/fontello/css/pm-product-pages.css', '', PM_SNAPPRO_VERSION, 'all'  );
	// Quick Edit
	//http://wpdreamer.com/2012/03/manage-wordpress-posts-using-bulk-edit-and-quick-edit/
	//http://www.janes.co.za/add-to-custom-post-type-quick-edit-on-wordpress/
	add_action( 'manage_pm_products_posts_columns', 'pm_snappro_add_column', 10, 2 );
	add_action( 'manage_pm_products_posts_custom_column', 'pm_snappro_reg_price_column', 10, 2 );
	add_action( 'manage_pm_products_posts_custom_column', 'pm_snappro_wpversion_column', 10, 2 );
	add_action( 'quick_edit_custom_box', 'pm_snappro_add_quick_edit', 10, 2 );
	add_action( 'bulk_edit_custom_box', 'pm_snappro_add_quick_edit', 10, 2 );
}
add_action( 'admin_init', 'pm_snappro_admin_font_init' );

/*--------------------------------------------
 * Special Template for custom post type
 --------------------------------------------*/
function pm_snappro_product_page( $template ) {
	global $wp_query;

	if( is_post_type_archive( 'pm_products' ) ) {
		$template = PM_SNAPPRO_PATH .'/modules/template/product_archive.php';
	} elseif( is_tax('type') ) {
		$template = PM_SNAPPRO_PATH .'/modules/template/product_tax.php';
	} elseif( ( get_query_var('post_type') == 'pm_products' ) || ( is_front_page() && is_singular( 'pm_products' ) ) ) {
		$template = PM_SNAPPRO_PATH .'/modules/template/product_single.php';
	}

	return $template;
}
add_filter( 'template_include', 'pm_snappro_product_page' );

/*--------------------------------------------
 * Add Column in post list
 --------------------------------------------*/
 // Called in line 19
function pm_snappro_add_column( $columns ) {
	$new_columns = array(
		'reg_price' => __('Price', 'pm-product-pages'),
		'wpversion' => __('WP Version', 'pm-product-pages')
	);
    return array_merge($columns, $new_columns);
}

function pm_snappro_reg_price_column( $column, $post_id ) {
	$meta_array = get_post_meta( $post_id, '_pm_snappro_post_meta' );
    $meta = ( isset( $meta_array[0] ) ) ? $meta_array[0] : '' ;
	switch ( $column ) {
		case 'reg_price':
			echo "<div id=\"current_price-" . $post_id . "\">$" . $meta['reg_price'] . "</div";
		break;
	}
}

function pm_snappro_wpversion_column( $column, $post_id ) {
	$meta_array = get_post_meta( $post_id, '_pm_snappro_post_meta' );
    $meta = ( isset( $meta_array[0] ) ) ? $meta_array[0] : '' ;
	switch ( $column ) {
		case 'wpversion':
			echo "<div id=\"wpversion-" . $post_id . "\">" . $meta['requirements'] . "</div";
		break;
	}
}

function pm_snappro_add_quick_edit( $column, $post_type ) {
	static $printNonce = TRUE;
	if( $printNonce ) {
		$printNonce = FALSE;
		wp_nonce_field( plugin_basename( __FILE__ ), 'price_qedit_nonce' );
	}
	?>
	<fieldset class="inline-edit-col-right inline-edit-price">
		<div class="inline-edit-col column-<?php echo $column;?>">
			<label class="inline-edit-group">
				<?php
				switch ( $column ) {
					case 'reg_price': ?>
						<div class="reg_price_col">
							<span class="title">Price $</span>
							<input type="text" name="reg_price" value="">
						</div>
					<?php
						break;
					case 'wpversion': ?>
						<div class="wpversion">
							<span class="title">WP Version</span>
							<input type="text" name="requirements" value="">
						</div>
						<?php
					break;
				}
				?>
			</label>
		</div>
	</fieldset>
	<?php
}

add_action( 'save_post', 'pm_snappro_quicksave', 10, 2 );
function pm_snappro_quicksave( $post_id, $post ) {
	// Skip for Autosave
	if( defined( 'DOING_AUTOSAVE') && DOING_AUTOSAVE )
		return $post_id;

	// Skip for revisions
	if( isset( $post->post_type ) && $post->post_type == 'revision' )
		return $post_id;

	switch( $post->post_type ) {
		case 'pm_products':
		if( array_key_exists( 'price_qedit_nonce', $_POST ) ) {
			$productmeta = get_post_meta( $post_id, '_pm_snappro_post_meta', TRUE );
			$qe_productmeta = array( 
				'reg_price' => sanitize_text_field( $_POST['reg_price'] ),
				'requirements' => sanitize_text_field( $_POST['requirements'] ),
				);
			$productmeta = array_replace( $productmeta, $qe_productmeta);
			update_post_meta( $post_id, '_pm_snappro_post_meta', $productmeta );
		}
		break;
	}
}

// Bulk edit AJAX (from admin.js)
add_action( 'wp_ajax_pm_snappro_save_bulk_edit', 'pm_snappro_process_save_bulk_edit' );
function pm_snappro_process_save_bulk_edit() {
	// Get posted variables
	$post_ids = ( isset( $_POST['post_ids'] ) && !empty( $_POST['post_ids'] ) ) ? $_POST['post_ids'] : array();
	$reg_price = ( isset( $_POST['reg_price']) && !empty( $_POST['reg_price'] ) ) ? sanitize_text_field( $_POST['reg_price'] ) : NULL;
	$requirements = ( isset( $_POST['wpversion'] ) && !empty( $_POST['wpversion'] ) ) ? sanitize_text_field( $_POST['wpversion'] ) : NULL;
	//file_put_contents('1snappro.log', print_r($post_ids, true)."\r\nPrice: ".print_r($reg_price, true)." Requirements: ".print_r($requirements, true));

	// If everything is in order...
	if( !empty( $post_ids ) && is_array( $post_ids ) ) {

		// Return if not the right meta
		if( !empty( $reg_price ) || !empty( $requirements ) ) {

		foreach ( $post_ids as $post_id ) {
			// Substitute null values for defaults
			$meta_array = get_post_meta( $post_id, '_pm_snappro_post_meta' );
    		$meta = ( isset( $meta_array[0] ) ) ? $meta_array[0] : '' ;
    		$reg_price = ( empty( $reg_price ) ) ? $meta['reg_price'] : $reg_price ;
    		$requirements = ( empty( $requirements ) ) ? $meta['requirements'] : $requirements ;

			$productmeta = get_post_meta( $post_id, '_pm_snappro_post_meta', TRUE );
			$qe_productmeta = array( 
				'reg_price' => $reg_price,
				'requirements' => $requirements,
				);
			$productmeta = array_replace( $productmeta, $qe_productmeta);
			update_post_meta( $post_id, '_pm_snappro_post_meta', $productmeta );
			//file_put_contents('2snapproproductmeta.log', print_r($productmeta, true)."\r\n----\r\n", FILE_APPEND);
		}
		}
	}
}

// Upsell shortcode button
add_shortcode( 'pm_upsell_btn', 'pm_snappro_upsell_btn' );
function pm_snappro_upsell_btn( $atts, $content = null ) {
	global $pm_snappro_opts;
	wp_enqueue_script( 'pm_ambr_cart', site_url( '/wp-content/plugins/pm-product-pages/modules/js/libs/cart.js' ), array('jquery'), false, true );
	$localize_array = array( 'ambrdomain' => $pm_snappro_opts['ambrdomain'], 'ambrfolder' => $pm_snappro_opts['ambrfolder'] );
	wp_localize_script( 'pm_ambr_cart', 'snappro_ambr_data', $localize_array );

	$pid = ( isset($_GET['pid']) && is_numeric($_GET['pid']) ) ? stripslashes($_GET['pid']) : '' ;
	$pname = ( isset($_GET['pname']) ) ? urldecode($_GET['pname']) : '' ;

	extract( shortcode_atts( array(
      'yespid' => '',
      'nopid' => $pid,
      'yestxt' => 'Yes! Join Now',
      'notxt' => 'No Thanks',
      'behavior' => 'addBasketExternal'
      // ...etc
      ), $atts ) );

	$notxt = ( stripos($notxt, '%%pname%%') !== FALSE ) ? str_replace('%%pname%%', $pname, $notxt) : $notxt ;

   if( !empty($pid) ) {
   	$nothanks = '<li class="order-opts decline"><a class="order-button" href="#" onclick="cart.'.$behavior.'(this,'.$nopid.'); return false;">'.$notxt.'</a></li>';
   }
	  	  
	  return <<<HTML
	  <ul class="sp-cta">
	  <script>if (typeof cart  == "undefined")
    document.write("<scr" + "ipt src=\'//{$pm_snappro_opts['ambrdomain']}/{$pm_snappro_opts['ambrfolder']}/application/cart/views/public/js/cart.js\'></scr" + "ipt>");
	</script>
	  <li class="order-opts accept"><a class="order-button" href="#" onclick="cart.{$behavior}(this,{$yespid}); return false;">{$yestxt}</a></li>
	  {$nothanks}
	  </ul>
HTML;
}
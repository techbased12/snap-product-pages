<?php 
// Register and enqueue admin scripts
add_action( 'admin_enqueue_scripts', 'pm_snappro_enqueue_scripts' );
function pm_snappro_enqueue_scripts() {
	$screen = get_current_screen();
	global $pm_snappro_opts;

	// Only enqueue if is custom post type admin
	if( $screen->post_type == 'pm_products' ) {
		wp_register_style( 'pm-snappro-admin-styles', PM_SNAPPRO_DIR.'modules/css/admin.joined.min.css', '', PM_SNAPPRO_VERSION, 'all' );
		wp_enqueue_style( 'pm-snappro-admin-styles' );
		//wp_enqueue_style( 'pm-snappro-datepicker', PM_SNAPPRO_DIR.'modules/css/libs/flat-blue-green/jquery-ui-1.10.4.custom.css', '', PM_SNAPPRO_VERSION, 'all' );

		wp_register_script( 'pm_snappro_admin_js', PM_SNAPPRO_DIR.'modules/js/build/admin.joined.min.js', array('jquery', 'jquery-ui-core','jquery-ui-datepicker'), PM_SNAPPRO_VERSION, true );
		wp_enqueue_script( 'pm_snappro_admin_js' );
		//wp_enqueue_script( 'pm_snappro_datepicker', PM_SNAPPRO_DIR.'modules/js/datepicker.js', array('jquery', 'jquery-ui-core','jquery-ui-datepicker'), PM_SNAPPRO_VERSION, true );
	}

}

// Register and enqueue front end scripts
add_action( 'wp_enqueue_scripts', 'pm_snappro_enqueue_front_scripts' );
function pm_snappro_enqueue_front_scripts() {
	global $post;
	global $pm_snappro_opts;
	if( !empty( $post->ID ) )
		$meta_array = get_post_meta( $post->ID, '_pm_snappro_post_meta' );
    $meta = ( isset( $meta_array[0] ) ) ? $meta_array[0] : '' ;

	// Only enqueue if is custom post type
	if( $post->post_type == 'pm_products' ) {
		wp_register_style( 'pm-snappro-front-styles', PM_SNAPPRO_DIR.'modules/css/front.joined.min.css', '', PM_SNAPPRO_VERSION, 'all' );
		wp_enqueue_style( 'pm-snappro-front-styles' );

		// Only enqueue if video type is mp4
		if( $meta['video_type'] == 'mp4' ) {
			wp_enqueue_script( 'wp-mediaelement', array('jquery') );
			wp_enqueue_style( 'wp-mediaelement' );
			//echo 'yes mp4';
		}
		// Only enqueue if aMember is used
		if( !empty( $meta['ambr_id'] ) ) {
			wp_enqueue_script( 'pm-snappro-front', PM_SNAPPRO_DIR.'modules/js/build/front.joined.min.js', array('jquery','jquery-ui-core', 'jquery-effects-highlight', 'jquery-ui-tooltip'), false, true );
			$js_data = array('ambrdomain' => $pm_snappro_opts['ambrdomain'], 'ambrfolder' => $pm_snappro_opts['ambrfolder'] );
			wp_localize_script('pm-snappro-front', 'snappro_ambr_data', $js_data );
		}
	}
}
?>
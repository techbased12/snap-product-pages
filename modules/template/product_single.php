<?php
//* Add custom body class to the head
add_filter( 'body_class', 'pm_snappro_single_body_class' );
function pm_snappro_single_body_class( $classes ) {
   $classes[] = 'pm-snappro-single';
   return $classes;	   
}

//* Force full width content layout
add_filter( 'genesis_site_layout', '__genesis_return_full_width_content' );

//* Remove the entry meta in the entry header (requires HTML5 theme support)
remove_action( 'genesis_entry_header', 'genesis_post_info', 12 );

//* Remove the entry title (requires HTML5 theme support)
remove_action( 'genesis_entry_header', 'genesis_do_post_title' );

//* Remove the entry header markup (requires HTML5 theme support)
remove_action( 'genesis_entry_header', 'genesis_entry_header_markup_open', 5 );
remove_action( 'genesis_entry_header', 'genesis_entry_header_markup_close', 15 );

//* Remove the post content (requires HTML5 theme support)
remove_action( 'genesis_entry_content', 'genesis_do_post_content' );

//* Remove the entry meta in the entry footer (requires HTML5 theme support)
remove_action( 'genesis_entry_footer', 'genesis_post_meta' );

add_action( 'genesis_entry_content', 'pm_snappro_entry_content' );
function pm_snappro_entry_content() {
	global $post;
	global $pm_snappro_opts;
	$meta_array = get_post_meta( $post->ID, '_pm_snappro_post_meta' );
    $meta = ( isset( $meta_array[0] ) ) ? $meta_array[0] : '' ;
    $ambrfolder = ( empty($meta['ambrfolder'] ) ) ? '' : esc_attr( $meta['ambrfolder'] ) ;
	$ambrpath = ( empty( $pm_snappro_opts['ambrpath'] ) ) ? '' : esc_attr( $pm_snappro_opts['ambrpath'] );
    $version = ( empty( $meta['version'] ) ) ? '' : esc_attr( $meta['version'] ) ;
    $wpversion = ( empty( $pm_snappro_opts['wpversion'] ) ) ? '' : esc_attr( $pm_snappro_opts['wpversion'] );
    $phpversion = ( empty( $pm_snappro_opts['phpversion'] ) ) ? '' : esc_attr( $pm_snappro_opts['phpversion'] );
    $updated = ( empty( $meta['updated'] ) ) ? '' : esc_attr( $meta['updated'] ) ;
    $requirements = ( empty( $meta['requirements'] ) ) ? esc_attr( $wpversion ) : esc_attr( $meta['requirements'] ) ;
	?>
	<div class="pm-snappro-product-feature">
		<div class="left">
			<div class="video-screenshot">
				<?php if( empty($meta['video_url']) ) { 
					if( has_post_thumbnail( $post->ID ) ) {
						the_post_thumbnail(  );
					} else {
						?>
						<h3>Not Ready</h3>
						<p>This product is not ready for publication. Upload a featured image or enter a video URL</p>
						<?php
					}
				} else {
					// Determine which type of video to display
				    switch( $meta['video_type'] ) {
				    	case 'yt' : ?>
				    		<iframe class="video528" src="<?php echo esc_url( $meta['video_url'] ); ?>?rel=0" frameborder="0" allowfullscreen></iframe>
					    	<?php  	break;
					    case 'vimeo' : ?>
					    	<iframe class="video528" src="<?php echo esc_url( $meta['video_url'] ); ?>?title=0&amp;byline=0&amp;portrait=0&amp;badge=0" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
					    	<?php 	break;
					    case 'mp4' :
					    $video = esc_url( $meta['video_url'] );
					    	?><div class="video528"><!--[if lt IE 9]><script>document.createElement("video");</script><![endif]-->
							<video class="wp-video-shortcode" style="width: 100%; height: 100%;" preload="metadata" controls="controls"><source type="video/mp4" src="<?php echo $video; ?>"> <a href="<?php echo $video; ?>"><?php echo $video; ?></a></video></div>
					    	<?php					    	
						    break; ?>
							<?php
				    }
					?>
					
				<?php } ?>
			</div>
			<div id="screenshots" class="screenshots">
				<?php 
				$args = array('post_type' => 'attachment', 'posts_per_page' => 30, 'post_parent' => $post->ID);
				$attachments = get_posts( $args );
				if( $attachments ) { 
					foreach ($attachments as $img ) {			
						echo wp_get_attachment_link( $img->ID, 'thumbnail', false, false );
					}
				} ?>
			</div>
			<div class="screenshot-nav">				
			</div>
			<div class="pm-snappro-desc">
				<h3>Features</h3>
				<?php the_content(  ); ?>
			</div>
		</div>
		<div class="right">
			<div class="product-name">
			<?php the_title( '<h1>', '</h1>', true );  ?>
			</div>
			<div class="product-pricing">				
				<div class="price">
					<?php if( empty( $meta['discount'] ) ) { ?>
					$<?php echo esc_attr( $meta['reg_price'] );
					} else { 
						if( empty( $meta['percentage'] ) ) { 
							$price = $meta['reg_price'] - $meta['discount'];
						} else {
							$disc = ( $meta['discount'] / 100 ) * $meta['reg_price'];
							$price = $meta['reg_price'] - $disc;
						}
							?>
					<strike>$<?php echo esc_attr( $meta['reg_price'] ); ?></strike> $<?php echo esc_attr( $price ); ?>
					<?php
					} ?>
				</div>
				<div class="order">
					<?php if( empty( $meta['ambr_id'] ) ) { ?>
						<a id="buy" href="<?php echo esc_url( $meta['order_url'] ); ?>">Buy Now</a>
					<?php } else {
						pm_snappro_ambr_buy_btn( $meta );	
					} ?>
				</div>
				<div class="cart-contents">
					<h3>Cart Contents</h3>
					<!-- Basket for aMember Shopping Cart -->
					<script type="text/javascript">
					jQuery(function(){cart.loadOnly();});
					</script>
					<div class="am-basket-preview"></div>
					<!-- End Basket for aMember Shopping Cart -->
				</div>
				<div class="product-details">
				<div class="license-info">
					<h3>License</h3>
					<ul>
					<?php 
					$terms = get_the_terms( $post->ID, 'type' );
					if( $terms && !is_wp_error( $terms ) ) {
						foreach ($terms as $term) {
							?>
							<li><?php echo $term->name; ?></li>
							<?php
						}
					}					
					?>
					</ul>
				</div>
				<div class="tech-info">
					<h3>Technical Information</h3>
					<p>Requirements:
						<ul>
							<li>PHP <?php echo $phpversion; ?> and up</li>
							<li>WordPress <?php echo $requirements; ?> and up</li>
						</ul>
					</p>
					<!-- <p>Current version: <?php echo $version; ?></p>
					<p>Date updated: <?php echo $updated; ?></p> -->
				</div>
			</div>			
		</div>
	</div>
	<?php
}


function pm_snappro_ambr_buy_button_upgrade( $ambr_id ) { ?>
	<a id="buy" href="http://www.pluginmill.com/save-money/?pid=<?php echo esc_attr( $ambr_id ); ?>">Buy This Plugin</a>
<?php
}

function pm_snappro_ambr_add_cart_button( $ambr_id ) { ?>
	<script>if (typeof cart  == "undefined")
	    document.write("<scr" + "ipt src=\'//www.pluginmill.com/account/application/cart/views/public/js/cart.js\'></scr" + "ipt>");
	</script>								
	<a id="buy" href="#" onclick="cart.addBasketExternal(this,<?php echo esc_attr( $ambr_id ); ?>); return false;">Add to Cart</a>
<?php
}

function pm_snappro_join_club_button($club, $membersonly) {
	if( $club && empty( $membersonly ) ) {
		$desc = '';
		$label = '<span class="small">Save On This & Future Plugins</span><br>Join The Club';
	} elseif ( ( $club && $membersonly ) || ( empty( $club ) && $membersonly ) ) {
		$desc = '<p class="membersonly-desc">Exclusive For Members Only <sup><i class="fa fa-question-circle"><span>Only club members can purchase this plugin.</span></i></sup></p>';
		$label = 'Join The Club<br><span class="small">To Buy At A Discount</span>';
	}
	echo $desc;
	?>
	<a id="clubbuy" href="https://www.pluginmill.com/account/signup/index/c/club">
	<?php echo $label; ?>
	</a>
<?php
}

function pm_snappro_ambr_buy_btn( $meta ) {
	// License type
	if( pm_snappro_license_type() === 'plr' ){
		pm_snappro_ambr_plr_btn( $meta['ambr_id'], $meta['club'], $meta['membersonly'], $meta['upsell'] );
	} elseif ( pm_snappro_license_type() === 'personal' ) {
		pm_snappro_ambr_personal_btn( $meta['ambr_id'], $meta['upsell'], get_the_title() );
	}
}

function pm_snappro_ambr_plr_btn( $ambr_id, $club, $membersonly, $upsell ) {
	// Check if product is for members only and club buttons
	if( !empty($membersonly) ) {
		// Check if member is currently logged in & has subscription
		if( class_exists('am4PluginsManager')) {
			$is_member = Am_Lite::getInstance()->haveSubscriptions(8);
		} else {
			include_once($ambrpath);
			$is_member = Am_Lite::getInstance()->haveSubscriptions(8);
		}
			if( $is_member ) {
				pm_snappro_ambr_add_cart_button( $ambr_id );
				echo "<p class='small'>" . Am_Lite::getInstance()->getName() . ",<br>Don't forget to use your member coupon.</p>";
			} else {
				// Visitor has no membership, Ask to join club
				pm_snappro_join_club_button( $club, $membersonly);
			}
	} else {
		// Available to non-members go ahead,
		// Display aMember add to cart button
		pm_snappro_ambr_add_cart_button( $ambr_id );

		// Show club upgrade button?
		if( !empty($club) ) {
			pm_snappro_join_club_button( $club, $membersonly);
		}
	}
}

function pm_snappro_ambr_personal_btn( $ambr_id, $upsell, $pname ) {
	global $pm_snappro_opts;
	$upsellurl = ( !empty( $pm_snappro_opts['personalupsell'] ) ) ? $pm_snappro_opts['personalupsell'] : '';

	// Upsell?
	if( $upsell && $upsell === 'on' ) { ?>
		<a id="buy" href="<?php echo $upsellurl; ?>?pid=<?php echo $ambr_id; ?>&pname=<?php echo urlencode( $pname ); ?>">Add to Cart</a>
	<?php } else {
		pm_snappro_ambr_add_cart_button( $ambr_id );
	}
}

function pm_snappro_license_type() {
	global $post;
	global $pm_snappro_opts;
	$terms = get_the_terms( $post->ID, 'type' );

	foreach ( $terms as $key => $term ) {
		if( $term->term_id == $pm_snappro_opts['plrtaxid'] ) {
			return 'plr';
		} elseif( $term->term_id == $pm_snappro_opts['personaltaxid'] ) {
			return 'personal';
		} else {
			return '';
		}
	}
}

genesis();
<?php
//* Add custom body class to the head
add_filter( 'body_class', 'pm_snappro_tax_body_class' );
function pm_snappro_tax_body_class( $classes ) {
   $classes[] = 'pm-snappro-tax';
   return $classes;	   
}

//* Force full width content layout
add_filter( 'genesis_site_layout', '__genesis_return_full_width_content' );

//* Remove breadcrumbs
remove_action( 'genesis_before_loop', 'genesis_do_breadcrumbs' );

//* Remove the default Genesis loop
remove_action( 'genesis_loop', 'genesis_do_loop' );

//* Add top widgets
add_action( 'genesis_after_header', 'pm_products_list_top' );

//* Add Static Content
function pm_products_list_top() {
	if( is_tax( 'type', 'private-label' ) ) {
		genesis_widget_area( 'plr-list', array(
			'before' => '<div class="plr-list-top widget-area"><div class="wrap">',
			'after'  => '</div></div>',
		) );
	} elseif( is_tax( 'type', 'personal' ) ) {
		genesis_widget_area( 'personal-list', array(
			'before' => '<div class="personal-list-top widget-area"><div class="wrap">',
			'after'  => '</div></div>',
		) );
	}
}

genesis();
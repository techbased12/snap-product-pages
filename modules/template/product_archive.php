<?php
//* Add custom body class to the head
add_filter( 'body_class', 'pm_snappro_archive_body_class' );
function pm_snappro_archive_body_class( $classes ) {
   $classes[] = 'pm-snappro-archive';
   return $classes;	   
}

//* Force full width content layout
add_filter( 'genesis_site_layout', '__genesis_return_full_width_content' );

//* Remove the entry meta in the entry header (requires HTML5 theme support)
remove_action( 'genesis_entry_header', 'genesis_post_info', 12 );

//* Remove the entry title (requires HTML5 theme support)
remove_action( 'genesis_entry_header', 'genesis_do_post_title' );

//* Remove loop
remove_action( 'genesis_loop', 'genesis_do_loop' );

//* Remove the post content (requires HTML5 theme support)
remove_action( 'genesis_entry_content', 'genesis_do_post_content' );

//* Remove the entry meta in the entry footer (requires HTML5 theme support)
remove_action( 'genesis_entry_footer', 'genesis_post_meta' );

add_action( 'genesis_loop', 'pm_custom_archive_loop' );
function pm_custom_archive_loop() {
	?>
	<article class="pm_products entry">
		<ul>
			<li>
				<a href="/type/personal">
				<img src="<?php echo plugin_dir_url( dirname(__FILE__) ); ?>img/Personal-use.png" alt="Personal Use">
				<h2>Personal Use</h2>
				</a>
			</li>
			<li>
				<a href="/type/private-label">
				<img src="<?php echo plugin_dir_url( dirname(__FILE__) ); ?>img/Privatelabel.png" alt="Private Label">
				<h2>Private Label</h2>
				</a>
			</li>
		</ul>
	</article>
	<?php
}

genesis();
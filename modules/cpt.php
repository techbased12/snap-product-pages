<?php
function pm_snappro_add_post_type() {
    global $pm_snappro_opts;
  $labels = array(
    'name' => _x('Snap Product Pages', 'post type general name', 'pm-snap-product-pages'),
    'singular_name' => _x('Product Page', 'post type singular name', 'pm-snap-product-pages'),
    'add_new' => _x('Add New', 'book', 'pm-snap-product-pages'),
    'add_new_item' => __('Add New Product Page', 'pm-snap-product-pages'),
    'edit_item' => __('Edit Product Page', 'pm-snap-product-pages'),
    'new_item' => __('New Product Page', 'pm-snap-product-pages'),
    'all_items' => __('All Product Pages', 'pm-snap-product-pages'),
    'view_item' => __('View Product Page', 'pm-snap-product-pages'),
    'search_items' => __('Search Product Pages', 'pm-snap-product-pages'),
    'not_found' =>  __('No Product Page found', 'pm-snap-product-pages'),
    'not_found_in_trash' => __('No Product Page found in Trash', 'pm-snap-product-pages'), 
    'parent_item_colon' => '',
    'menu_name' => __('Product Pages', 'pm-snap-product-pages')

  );
  $taxonomies = array(
    'type',
    'features'
    );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true, 
    'show_in_menu' => true,
    'show_admin_bar' => true,
    'query_var' => true,
    'rewrite' =>  array( 'slug' => $pm_snappro_opts['productslug'] ),
    'capability_type' => 'post',
    'has_archive' => true, 
    'hierarchical' => false,
    'menu_position' => null,
    'register_meta_box_cb' => 'pm_snappro_metaboxes',
    'taxonomies' => $taxonomies,
    'supports' => array( 'title', 'editor', 'thumbnail' )
  ); 
  register_post_type( 'pm_products', $args);
}
add_action( 'init', 'pm_snappro_add_post_type' );

if ( ! function_exists( 'pm_snappro_type_taxonomy' ) ) {

// Register Custom Taxonomy
function pm_snappro_type_taxonomy() {
    global $pm_snappro_opts;

    $labels = array(
        'name'                       => _x( 'Types', 'Taxonomy General Name', 'pm-product-pages' ),
        'singular_name'              => _x( 'Type', 'Taxonomy Singular Name', 'pm-product-pages' ),
        'menu_name'                  => __( 'Type', 'pm-product-pages' ),
        'all_items'                  => __( 'All Types', 'pm-product-pages' ),
        'parent_item'                => __( 'Parent Type', 'pm-product-pages' ),
        'parent_item_colon'          => __( 'Parent Type:', 'pm-product-pages' ),
        'new_item_name'              => __( 'New Type Name', 'pm-product-pages' ),
        'add_new_item'               => __( 'Add New Type', 'pm-product-pages' ),
        'edit_item'                  => __( 'Edit Type', 'pm-product-pages' ),
        'update_item'                => __( 'Update Type', 'pm-product-pages' ),
        'separate_items_with_commas' => __( 'Separate types with commas', 'pm-product-pages' ),
        'search_items'               => __( 'Search Types', 'pm-product-pages' ),
        'add_or_remove_items'        => __( 'Add or remove types', 'pm-product-pages' ),
        'choose_from_most_used'      => __( 'Choose from the most used types', 'pm-product-pages' ),
        'not_found'                  => __( 'Not Found', 'pm-product-pages' ),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => true,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
    );
    register_taxonomy( 'type', $pm_snappro_opts['productslug'], $args );

}

// Hook into the 'init' action
add_action( 'init', 'pm_snappro_type_taxonomy', 0 );

}

if ( ! function_exists( 'pm_snappro_feature_taxonomy' ) ) {

// Register Custom Taxonomy
function pm_snappro_feature_taxonomy() {
    global $pm_snappro_opts;

    $labels = array(
        'name'                       => _x( 'Features', 'Taxonomy General Name', 'pm-product-pages' ),
        'singular_name'              => _x( 'Feature', 'Taxonomy Singular Name', 'pm-product-pages' ),
        'menu_name'                  => __( 'Feature', 'pm-product-pages' ),
        'all_items'                  => __( 'All Features', 'pm-product-pages' ),
        'parent_item'                => __( 'Parent Feature', 'pm-product-pages' ),
        'parent_item_colon'          => __( 'Parent Feature:', 'pm-product-pages' ),
        'new_item_name'              => __( 'New Feature Name', 'pm-product-pages' ),
        'add_new_item'               => __( 'Add New Feature', 'pm-product-pages' ),
        'edit_item'                  => __( 'Edit Feature', 'pm-product-pages' ),
        'update_item'                => __( 'Update Feature', 'pm-product-pages' ),
        'separate_items_with_commas' => __( 'Separate features with commas', 'pm-product-pages' ),
        'search_items'               => __( 'Search Features', 'pm-product-pages' ),
        'add_or_remove_items'        => __( 'Add or remove features', 'pm-product-pages' ),
        'choose_from_most_used'      => __( 'Choose from the most used features', 'pm-product-pages' ),
        'not_found'                  => __( 'Not Found', 'pm-product-pages' ),
    );
    $args = array(
        'labels'                     => $labels,
        'hierarchical'               => false,
        'public'                     => true,
        'show_ui'                    => true,
        'show_admin_column'          => true,
        'show_in_nav_menus'          => true,
        'show_tagcloud'              => true,
    );
    register_taxonomy( 'features', $pm_snappro_opts['productslug'], $args );

}

// Hook into the 'init' action
add_action( 'init', 'pm_snappro_feature_taxonomy', 0 );

}

// Add metaboxes for this post type. register_meta_box_cb callback 
function pm_snappro_metaboxes() {
    global $pm_snappro_opts;
    add_meta_box( 'pricing', 'Pricing & Ordering', 'pm_snappro_mb_pricing', 'pm_products', 'normal', 'high' );
    add_meta_box( 'tech', 'Technical info', 'pm_snappro_mb_tech', 'pm_products', 'normal', 'high' );
    add_meta_box( 'marketing', 'Sales and Marketing Resources', 'pm_snappro_mb_marketing', 'pm_products', 'normal', 'high' );
    add_meta_box( 'shots', 'Product Shots and Images', 'pm_snappro_mb_shots', 'pm_products', 'normal', 'high' );
}

// Metabox callback
function pm_snappro_mb_pricing() {
    wp_nonce_field( 'save_meta', '_pm_snappro_save_meta' );
    global $post;
    $meta_array = get_post_meta( $post->ID, '_pm_snappro_post_meta' );
    $meta = ( isset( $meta_array[0] ) ) ? $meta_array[0] : '' ;
    $reg_price = ( isset( $meta['reg_price'] ) ) ? sanitize_text_field( $meta['reg_price'] ) : '';
    $discount = ( isset( $meta['discount'] ) ) ? sanitize_text_field( $meta['discount'] ) : '';
    $order_url = ( isset( $meta['order_url'] ) ) ? esc_url( $meta['order_url'] ) : '';
    $ambr_id = ( isset( $meta['ambr_id'] ) ) ? sanitize_text_field( $meta['ambr_id'] ) : '';
    $percentage = ( isset( $meta['percentage'] ) ) ? sanitize_text_field( $meta['percentage'] ) : '';
    $upsell = ( isset( $meta['upsell'] ) ) ? sanitize_text_field( $meta['upsell'] ) : '';
    ?>
    <p>
        <label for="reg_price">Regular price:</label> $<input type="text" id="reg_price" class="pricing" name="reg_price" value="<?php echo $reg_price; ?>">
    </p>
    <p>
        <label for="discount">Discount to apply: </label> <input type="text" id="discount" class="pricing" name="discount" value="<?php echo $discount; ?>"> <input type="checkbox" id="percentage" class="percentage" name="percentage" <?php checked( $percentage, 'on', true ); ?>> <label for="percentage"><small>Check if percentage</small></label>
    </p>
    <p>
        <label for="order_url">Order link:</label> <input type="text" id="order_url" class="orderinfo regular-text" name="order_url" value="<?php echo $order_url; ?>">
    </p>
    <p>
        <label for="ambr_id">aMember product ID:</label> <input type="text" id="ambr_id" class="orderinfo small-text" name="ambr_id" value="<?php echo $ambr_id ?>"> <small>Only if you use aMember</small>
    </p>
    <p>
        <label for="upsell">Enable Upsell: <input type="checkbox" id="upsell", class="upsell" name="upsell" <?php checked( $upsell, 'on', true ); ?>></label> <small>Make sure upsell URL is entered in Settings</small>
    </p>
    <?php
}
function pm_snappro_mb_tech() {
    wp_nonce_field( 'save_meta', '_pm_snappro_save_meta' );
    global $post;
    $meta_array = get_post_meta( $post->ID, '_pm_snappro_post_meta' );
    $meta = ( isset( $meta_array[0] ) ) ? $meta_array[0] : '' ;
    $version = ( isset( $meta['version'] ) ) ? sanitize_text_field( $meta['version'] ) : '';
    $updated = ( isset( $meta['updated'] ) ) ? sanitize_text_field( $meta['updated'] ) : '';
    $requirements = ( isset( $meta['requirements'] ) ) ? sanitize_text_field( $meta['requirements'] ) : '';
    ?>
    <p>
        <label for="version">Current version:</label> <input type="text" id="version" class="tech" name="version" value="<?php echo $version; ?>">
    </p>
    <p>
        <label for="updated">Date updated:</label> <input type="text" id="updated" class="updated" name="updated" data-date-format="dd-mm-yyyy" value="<?php echo $updated; ?>">
    </p>
    <p>
        <label for="requirements">Requirements:</label> <input type="text" id="requirements" class="tech" name="requirements" value="<?php echo $requirements; ?>">
    </p>
    <?php
}
function pm_snappro_mb_marketing() { 
    global $post;   
    $all_posts = get_posts('post_type=pm_products&post_status=any&showposts=-1&orderby=name&order=ASC');
    wp_nonce_field( 'save_meta', '_pm_snappro_save_meta' );
    $meta_array = get_post_meta( $post->ID, '_pm_snappro_post_meta' );
    $meta = ( isset( $meta_array[0] ) ) ? $meta_array[0] : '' ;
    $video_url = ( isset( $meta['video_url'] ) ) ? sanitize_text_field( $meta['video_url'] ) : '';
    $video_type = ( isset( $meta['video_type'] ) ) ? sanitize_text_field( $meta['video_type'] ) : '';
    $club = ( isset( $meta['club'] ) ) ? sanitize_text_field( $meta['club'] ) : '';
    $membersonly = ( isset( $meta['membersonly'] ) ) ? sanitize_text_field( $meta['membersonly'] ) : '';
    ?>
    <p><label for="video_url">Video URL</label> <input type="text" id="video_url" class="marketing regular-text" name="video_url" value="<?php echo $video_url; ?>"></p>
    <p><label for="video_type">Video type:</label> 
        <input type="radio" id="yt" name="video_type" value="yt" <?php checked( $video_type, 'yt', true ); ?>><label onclick="" class="btn" for="yt">Youtube</label> 
        <input type="radio" id="vimeo" name="video_type" value="vimeo" <?php checked( $video_type, 'vimeo', true ); ?>><label onclick="" class="btn" for="vimeo">Vimeo</label> 
        <input type="radio" id="mp4" name="video_type" value="mp4" <?php checked( $video_type, 'mp4', true ); ?>><label onclick="" class="btn" for="mp4">MP4</label> 
    </p>
    <p>
        <label for="club">Display club button</label> <input type="checkbox" id="club" class="marketing" name="club" <?php checked( $club, 'on', true );?>>
        <label for="membersonly">Club Member's Only</label> <input type="checkbox" id="membersonly" class="marketing" name="membersonly" <?php checked( $membersonly, 'on', true );?>>
    </p>
    <p>
        <label for="related">Related products</label>
        <select id="related" name="related">
            <option value="">None</option>
            <?php 
            foreach( $all_posts as $single_post ){
                echo '<option value="'.$single_post->ID.'">'.$single_post->post_title.'</option>';
            }
            ?>
        </select>
        <div id="related_items">
            <ul>
            <?php
            if( !empty( $meta['related'] ) ) {
            foreach ( $meta['related'] as $key => $rpost ) { ?>
                <li><span class="related_item"><?php echo get_the_title( $rpost ); ?> (<?php echo $rpost; ?>)<input type="hidden" name="related[]" value="<?php echo $rpost ?>" class="select_related">
                <span id="product-<?php echo $rpost; ?>" class="deleteItem">X</span></span>
            <?php }
            } ?>
            </ul>
        </div>
    </p>
    <?php
}
function pm_snappro_mb_shots() {
    global $post;
    wp_nonce_field( 'save_meta', '_pm_snappro_save_meta' );
    $meta_array = get_post_meta( $post->ID, '_pm_snappro_post_meta' );
    $meta = ( isset( $meta_array[0] ) ) ? $meta_array[0] : '' ;
    ?>
    <p>Upload all your product images here. They will be used under the featured image or video.</p>
    <a href="#" id="pm_snappro_productimg_upload" class="button">Upload Product Images</a>
    <div id="pm_snappro_product_images">
        <ul>
        <?php
        if( !empty( $meta['product_shots'] ) ) {
            foreach ( $meta['product_shots'] as $product_shot => $image ) {
                $image_id = $image['id'];
                ?>
                <li class="product_shot"><div class="pimage">
                    <img class="shot" src="<?php echo $image['url']; ?>">
                    <span class="deleteImg">x</span>
                    <input type="hidden" name="product_shots[<?php echo $image_id; ?>][url]" value="<?php echo $image['url']; ?>">
                    <input type="hidden" name="product_shots[<?php echo $image_id; ?>][id]" value="<?php echo $image['id']; ?>">
                </div></li>
                <?php
            }
        }
        ?>
        </ul>
    </div>
    <?php
}

/*--------------------------------------------
 * Save Metaboxes
 *--------------------------------------------*/
add_action( 'save_post', 'pm_snappro_save_meta' );
function pm_snappro_save_meta( $post_id ) {
    // Check if autosave is running
    if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
        return $post_id;
    }
    // If is saving
    if( !isset($_POST['_pm_snappro_save_meta']) ){
        return;
    }
    // Verify nonce
    if ( !empty($_POST['_pm_snappro_save_meta']) && !wp_verify_nonce( $_POST['_pm_snappro_save_meta'], 'save_meta' ) ) {
        echo 'Sorry, nonce did not verify.';
        exit;
    }
    // Verify permissions
    if ( !current_user_can( 'edit_page', $post_id ) )
        return;

    // Validate and Sanitize
    // Date validator
    function pm_snappro_validatedate($date, $format = 'j M, Y') {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }
    // Validate array
    function pm_snappro_validatepics($array) {
        foreach ($array as $key => $value) {
            if( filter_var( $value['url'], FILTER_VALIDATE_URL ) ) {
                $url = esc_html( $value['url'] );
                $a = array('url'=>$url);
                print_r($a);
                echo '<hr>';
                print_r($value['url']);
            }
        }        
    }
    // Accepted video types
    $allowed_video = array( 'yt', 'vimeo', 'mp4' );

    $valid_reg_price = ( isset( $_POST['reg_price'] ) && is_numeric( $_POST['reg_price'] ) == true ) ? floatval( $_POST['reg_price'] ) : '' ;
    $valid_discount = ( isset( $_POST['discount'] ) && is_numeric( $_POST['discount'] ) == true ) ? floatval( $_POST['discount'] ) : '' ;
    $valid_percentage = ( isset( $_POST['percentage'] ) && $_POST['percentage'] == 'on' ) ? $_POST['percentage'] : '' ;
    $valid_order_url = ( isset( $_POST['order_url'] ) ) ? esc_url_raw( $_POST['order_url'] ) : 'Invalid URL' ;
    $valid_ambr_id = ( isset( $_POST['ambr_id'] ) && is_numeric( $_POST['ambr_id'] ) == true ) ? intval( $_POST['ambr_id'] ) : '' ;
    $valid_upsell = ( isset( $_POST['upsell'] ) && $_POST['upsell'] == 'on' ) ? $_POST['upsell'] : '' ;
    $valid_version = ( isset( $_POST['version'] ) ) ? stripslashes( $_POST['version'] ) : '' ;
    $valid_updated = ( isset( $_POST['updated'] ) && pm_snappro_validatedate($_POST['updated'], 'j M, Y') == 1 ) ? $_POST['updated'] : 'Invalid date' ;
    $valid_requirements = ( isset( $_POST['requirements'] ) ) ? stripslashes( $_POST['requirements'] ) : '' ;
    $valid_video_url = ( isset( $_POST['video_url'] ) ) ? esc_url_raw( $_POST['video_url'] ) : 'Invalid URL' ;
    $valid_video_type = ( isset( $_POST['video_type'] ) && in_array( $_POST['video_type'], $allowed_video) == 1 ) ? $_POST['video_type'] : '' ;
    $valid_club = ( isset( $_POST['club'] ) && $_POST['club'] == 'on' ) ? $_POST['club'] : '' ;
    $valid_membersonly = ( isset( $_POST['membersonly'] ) && $_POST['membersonly'] == 'on' ) ? $_POST['membersonly'] : '' ;
    $valid_related = ( !empty( $_POST['related'] ) && $_POST['related'] === array_filter($_POST['related'], 'is_numeric') ) ? $_POST['related'] : '';
    if( isset( $_POST['product_shots'] ) ) {
        foreach ($_POST['product_shots'] as $product_shot => $img) {
            $id = $img['id'];
            $valid_product_shots[$id]['url'] = ( isset( $img['url'] ) && filter_var( $img['url'], FILTER_VALIDATE_URL ) == true ) ? esc_url_raw( $img['url'] ) : '' ;
            $valid_product_shots[$id]['id'] = ( isset( $img['id'] ) && is_numeric( $img['id'] ) == true ) ? intval( $img['id'] ) : '';
        }
    } else {
        $valid_product_shots = '';
    }


    // Re-build Metadata Array
    $pm_snappro_meta_array = array(
        'reg_price' => $valid_reg_price,
        'discount' => $valid_discount,
        'percentage' => $valid_percentage,
        'order_url' => $valid_order_url,
        'ambr_id' => $valid_ambr_id,
        'upsell' => $valid_upsell,
        'version' => $valid_version,
        'updated' => $valid_updated,
        'requirements' => $valid_requirements,
        'video_url' => $valid_video_url,
        'video_type' => $valid_video_type,
        'club' => $valid_club,
        'membersonly' => $valid_membersonly,
        'related' => $valid_related,
        'product_shots' => $valid_product_shots
        );

    // Save metadata
    update_post_meta( $post_id, '_pm_snappro_post_meta', $pm_snappro_meta_array );

    /*
    print_r($_POST);
    echo '<hr>';
    print_r($pm_snappro_meta_array);
    exit();
    */
}